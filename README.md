# Atentu

## What?
A MoTD generator!

Atentu is Esperanto for "Heads Up", which is exactly what a good dynamic MoTD should do, give you a "Heads Up" on your system. That's all the better if what generates said MoTD is simple and easy to use, which hopefully everything herein is.

## Info
Atentu is split into two parts, a generalize system info parsing library, and the actual Atentu program which leverages said library.
The library is intended to be used as is with fennel, but can also be compile into a Lua library and sourced in Lua code.

This is to say that Atentu is an example MoTD written using a new Lua system info library!

## Building
From the root of this directory you can do the following:
```
-- Compile Atentu to a binary
make compile-bin

-- Instal Atentu binary
make install-bin

-- Compile Atentu to Lua
make compile-lua

-- Install Atentu Lua file
make install-lua

-- Compile sysinfo to Lua
make compile-sysinfo

-- Install sysinfo to Lua share directories
make install-sysinfo
```
Additionally you can pass LUAV=5.x to install/compile with a specific Lua version.

### Depends:
df - for disc usage, parsing proc provided inaccurate
ip - for system ip info, parsing proc is possible here, so this dependency may eventually be removed
Alpine: coreutils (busbox df doesn't provide output=size)

## Examples:
A simple Lua and Fennel example can be found in the examples directory. The Lua version is a Fennel compiled one, so it's lacking the human touch (arbitrary spacing choices), but is just as functional/viable.

## Gotchas:
Disc info is produced by parsing the output from df, that should be the only external call. All other data is pulled from /procfs directly.

## TODO:

- [] Rock Build
- [] Alpine Package
- [] Atentu Config (build more flexibility)
- [] API Info