package = "atentu"
version = "0.2-1"
source = {
   url = "git+https://gitlab.com/durrendal/Atentu",
   tag = "0.2",
}
description = {
   summary = "A MOTD Generator",
   detailed = [[
Currently supports the following data points:
  - Memory Usage & Total
  - Swap Usage & Total
  - Disc Usage & Totals
  - Load Average
  - Uptime
  - IP Configuration by Interface
  - Hostname
  - CPU Temp
   ]],
   homepage = "http://lambdacreate/dev/atentu",
   license = "GPL 3.0"
}
dependencies = {
   "lua >= 5.1, < 5.4",
}
supported_platforms = { "linux" }
build = {
   type = "builtin",
   modules = {
      atentu = "src/atentu.lua"
   },
}
