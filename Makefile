#Alpine provides Lua 5.3 as Lua5.3, Lua is by default symlinked to Lua 5.1
#This can be changed to simply lua as needed/desired
LUA ?= lua5.3
LUAV = 5.3
LUA_SHARE=/usr/share/lua/$(LUAV)
#On Debian /usr/lib/lib/x86_64-linux-gnu/liblua5.3.a would be used, Alpine packages it a little differently, so we use the following
STATIC_LUA_LIB ?= /usr/lib/liblua-5.3.so.0.0.0
LUA_INCLUDE_DIR ?= /usr/include/lua5.3
DESTDIR=/usr/local/bin

compile-lua:
	fennel --compile examples/atentu.fnl > examples/atentu
	sed -i '1 i\-- Author: Will Sinatra <wpsinatra@gmail.com> | License: GPLv3' examples/atentu
	sed -i '1 i\#!/usr/bin/$(LUA)' examples/atentu

install-lua:
	install ./examples/atentu -D $(DESTDIR)/atentu

compile-atentu-lib:
	fennel --compile src/atentu.fnl > src/atentu.lua

install-atentu-lib:
	install ./src/atentu.lua -D $(LUA_SHARE)/atentu.lua

compile-bin:
	mkdir BUILD
	cp examples/atentu.fnl BUILD/atentu-bin.fnl
	fennel --compile src/atentu.fnl > BUILD/atentu.lua
	cd BUILD && fennel --compile-binary --require-as-include atentu-bin.fnl atentu-bin $(STATIC_LUA_LIB) $(LUA_INCLUDE_DIR)

install-bin:
	install atentu-bin -D $(DESTDIR)/atentu

clean:
	rm -rf BUILD
