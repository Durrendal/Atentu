local atentu = {}
local function split(val, check)
  if (check == nil) then
    local check0 = "%s"
  else
  end
  local t = {}
  for str in string.gmatch(val, ("([^" .. check .. "]+)")) do
    table.insert(t, str)
  end
  return t
end
local function fmap(file, splitvar, map)
  local retval = ""
  for line in io.lines(file) do
    local dat = split(line, splitvar)
    if (dat[1] == map) then
      retval = dat[2]
    else
    end
  end
  return retval
end
local function addontbl(tbl)
  local ttot = 0
  for k, v in pairs(tbl) do
    if (type(v) == "table") then
      local mean = (addontbl(v) / #v)
      ttot = (mean + ttot)
    else
      ttot = (ttot + v)
    end
  end
  return ttot
end
atentu.mem = function(typ)
  local to = fmap("/proc/meminfo", " ", "MemTotal:")
  local total = (to * 0.000977)
  local fr = fmap("/proc/meminfo", " ", "MemFree:")
  local free = (fr * 0.000977)
  local bf = fmap("/proc/meminfo", " ", "Buffers:")
  local chd = fmap("/proc/meminfo", " ", "Cached:")
  local used = ((to - fr - bf - chd) * 0.000977)
  if (typ == "total") then
    return (total - (total % 1))
  elseif (typ == "free") then
    return (free - (free % 1))
  elseif (typ == "used") then
    return (used - (used % 1))
  else
    return nil
  end
end
atentu.swap = function(typ)
  local to = fmap("/proc/meminfo", " ", "SwapTotal:")
  local total = (to * 0.000977)
  local fr = fmap("/proc/meminfo", " ", "SwapFree:")
  local free = (fr * 0.000977)
  local chd = fmap("/proc/meminfo", " ", "SwapCached:")
  local used = ((to - fr - chd) * 0.000977)
  if (typ == "total") then
    return (total - (total % 1))
  elseif (typ == "free") then
    return (free - (free % 1))
  elseif (typ == "used") then
    return (used - (used % 1))
  else
    return nil
  end
end
atentu.disc = function(typ, path)
  if (typ == "total") then
    local total = io.popen(("df -h --output=size " .. path .. " | tail -n 1"))
    local tr = total:read("*a")
    local clean = string.gsub(tr, "[\13\n]", "")
    local ret = string.gsub(clean, " ", "")
    return ret
  elseif (typ == "used") then
    local used = io.popen(("df -h --output=used " .. path .. " | tail -n 1"))
    local ur = used:read("*a")
    local clean = string.gsub(ur, "[\13\n]", "")
    local ret = string.gsub(clean, " ", "")
    return ret
  elseif (typ == "free") then
    local free = io.popen(("df -h --output=size " .. path .. " | tail -n 1"))
    local fr = free:read("*a")
    local clean = string.gsub(fr, "[\13\n]", "")
    local ret = string.gsub(clean, " ", "")
    return ret
  else
    return nil
  end
end
atentu.loadavg = function(typ)
  local avg = io.open("/proc/loadavg")
  local avg0 = avg:read("*a")
  local dat = split(avg0, " ")
  if (typ == 1) then
    return dat[1]
  elseif (typ == 5) then
    return dat[2]
  elseif (typ == 15) then
    return dat[3]
  elseif (typ == 6) then
    return (dat[1] .. " " .. dat[2])
  elseif (typ == 21) then
    return (dat[1] .. " " .. dat[2] .. " " .. dat[3])
  elseif (typ == 16) then
    return (dat[1] .. " " .. dat[3])
  elseif (typ == 20) then
    return (dat[2] .. " " .. dat[3])
  else
    return nil
  end
end
atentu.uptime = function(typ)
  local up = io.open("/proc/uptime")
  local up0 = up:read("*a")
  local dat = split(up0, " ")
  local ctime = (dat[1] - (dat[1] % 1))
  local d = (((ctime / 60) / 60) / 24)
  local h = (((ctime / 60) / 60) % 24)
  local m = ((ctime / 60) % 24)
  local s = (ctime % 60)
  local d1 = (d - (d % 1))
  local h1 = (h - (h % 1))
  local m1 = (m - (m % 1))
  local s1 = (s - (s % 1))
  if (typ == "d") then
    local dat0 = split(d1, ".")
    return (dat0)[1]
  elseif (typ == "h") then
    local dat0 = split(h1, ".")
    return (dat0)[1]
  elseif (typ == "m") then
    local dat0 = split(m1, ".")
    return (dat0)[1]
  elseif (typ == "s") then
    local dat0 = split(s1, ".")
    return (dat0)[1]
  else
    return nil
  end
end
atentu.name = function()
  local h = io.open("/etc/hostname")
  local h0 = h:read("*a")
  local h1 = string.gsub(h0, "[\13\n]", "")
  return h1
end
atentu.ip = function(dev)
  local i = io.popen(("ip -br addr | grep " .. dev .. " | awk '{print $3}'"))
  local i0 = i:read("*a")
  local i1 = string.gsub(i0, "[\13\n]", "")
  return i1
end
atentu.temp = function()
  local avgtemp = 0
  do
    local zones = io.popen("ls /sys/class/thermal | grep thermal_zone")
    local temps = {}
    for line in zones:lines() do
      local f = io.open(("/sys/class/thermal/" .. line .. "/temp"), "r")
      local function close_handlers_10_auto(ok_11_auto, ...)
        f:close()
        if ok_11_auto then
          return ...
        else
          return error(..., 0)
        end
      end
      local function _10_()
        return table.insert(temps, (tonumber(f:read()) / 1000))
      end
      close_handlers_10_auto(_G.xpcall(_10_, (package.loaded.fennel or debug).traceback))
    end
    for k, v in pairs(temps) do
      avgtemp = (avgtemp + v)
    end
    avgtemp = (avgtemp / #temps)
  end
  return avgtemp
end
atentu.local_users_today = function()
  local us = io.popen("grep \"pam_unix(login:session)\" /var/log/auth.log | grep $(date +%Y-%m-%d)")
  for line in us:lines() do
    local ul = split(line, " ")
    print(((split(ul[10], "("))[1] .. ": " .. ul[1] .. ":" .. ul[2]))
  end
  return nil
end
atentu.sshd_users_today = function(verbose_3f)
  local us = io.popen("grep -E 'sshd\\[([0-9])+\\]: Accepted password for' /var/log/auth.log | grep $(date +%Y-%m-%d)")
  for line in us:lines() do
    if (verbose_3f == true) then
      local ul = split(line, " ")
      print(((split(ul[8], "("))[1] .. ": " .. ul[1] .. ":" .. ul[2] .. " " .. ul[10]))
    else
      local ul = split(line, " ")
      print(((split(ul[8], "("))[1] .. ": " .. ul[1] .. ":" .. ul[2]))
    end
  end
  return nil
end
return atentu
