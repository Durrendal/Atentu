(local atentu {})
;;Author: Will Sinatra <wpsinatra@gmail.com>
;;System Info Library
;;Version: v0.2 | GPLv3

;;===== Utils =====
;;(split line delim_char)
(fn split [val check]
  (if (= check nil)
      (do
        (local check "%s")))
  (local t {})
  (each [str (string.gmatch val (.. "([^" check "]+)"))]
    (do
      (table.insert t str)))
  t)

;;(fmap "/path/to/file" delim_char ctrl_string)
(fn fmap [file splitvar map]
  (var retval "")
  (each [line (io.lines file)]
    (do
      (var dat (split line splitvar))
      (if (= (. dat 1) map)
          (do
            (set retval (. dat 2))))))
  retval)

(fn addontbl [tbl]
  "Sum values in a table"
  (var ttot 0)
  (each [k v (pairs tbl)]
    (do
      (if (= (type v) "table")
          (do
            (let
                [mean (/ (addontbl v) (length v))]
              (set ttot (+ mean ttot))))
          (set ttot (+ ttot v)))))
  ttot)

;;(mem "total") (mem "free") (mem "used")
(fn atentu.mem [typ]
  (let
      [to (fmap "/proc/meminfo" " " "MemTotal:")
       total (* to .000977)
       fr (fmap "/proc/meminfo" " " "MemFree:")
       free (* fr .000977)
       bf (fmap "/proc/meminfo" " " "Buffers:")
       chd (fmap "/proc/meminfo" " " "Cached:")
       used (* (- to fr bf chd) .000977)]
    (if
     (= typ "total")
     (- total (% total 1))
     (= typ "free")
     (- free (% free 1))
     (= typ "used")
     (- used (% used 1)))))

;;(swap "total") (swap "free") (swap "used")
(fn atentu.swap [typ]
  (let
      [to (fmap "/proc/meminfo" " " "SwapTotal:")
       total (* to .000977)
       fr (fmap "/proc/meminfo" " " "SwapFree:")
       free (* fr .000977)
       chd (fmap "/proc/meminfo" " " "SwapCached:")
       used (* (- to fr chd) .000977)]
    (if
     (= typ "total")
     (- total (% total 1))
     (= typ "free")
     (- free (% free 1))
     (= typ "used")
     (- used (% used 1)))))

;;(disc "total" "/") (disc "free" "/")
(fn atentu.disc [typ path]
  (if
   (= typ "total")
   (do
     (let
         ;;/sys/block/sdx/capacity (/ (/ (/ cap 1024) 1024) 1024) <- to get GB
         ;;/sys/block/sdx/sdxpx/size <- cap of part
         [total (io.popen (.. "df -h --output=size " path " | tail -n 1"))
          tr (total:read "*a")
          clean (string.gsub tr "[\r\n]" "")
          ret (string.gsub clean " " "")]
       ret))
   (= typ "used")
   (do
     (let
         [used (io.popen (.. "df -h --output=used " path " | tail -n 1"))
          ur (used:read "*a")
          clean (string.gsub ur "[\r\n]" "")
          ret (string.gsub clean " " "")]
       ret))
   (= typ "free")
   (do
     (let
         [free (io.popen (.. "df -h --output=size " path " | tail -n 1"))
          fr (free:read "*a")
          clean (string.gsub fr "[\r\n]" "")
          ret (string.gsub clean " " "")]
       ret))))

;;(loadavg x) {1,5,16,6,16,20,21}
(fn atentu.loadavg [typ]
  (local avg (io.open "/proc/loadavg"))
  (local avg (avg:read "*a"))
  (local dat (split avg " "))
  (if
   (= typ 1)
   (. dat 1)
   (= typ 5)
   (. dat 2)
   (= typ 15)
   (. dat 3)
   (= typ 6)
   (.. (. dat 1) " " (. dat 2))
   (= typ 21)
   (.. (. dat 1) " " (. dat 2) " " (. dat 3))
   (= typ 16)
   (.. (. dat 1) " " (. dat 3))
   (= typ 20)
   (.. (. dat 2) " " (. dat 3))))

;;(uptime "d") (uptime "h") (uptime "m") (uptime "s")
(fn atentu.uptime [typ]
  (let
      [up (io.open "/proc/uptime")
       up (up:read "*a")
       dat (split up " ")
       ctime (- (. dat 1) (% (. dat 1) 1))
       d (/ (/ (/ ctime 60) 60) 24)
       h (% (/ (/ ctime 60) 60) 24)
       m (% (/ ctime 60) 24)
       s (% ctime 60)
       d1 (- d (% d 1))
       h1 (- h (% h 1))
       m1 (- m (% m 1))
       s1 (- s (% s 1))]
    (if
     (= typ "d")
     (do
       (local dat (split d1 "."))
       (. dat 1))
     (= typ "h")
     (do
       (local dat (split h1 "."))
       (. dat 1))
     (= typ "m")
     (do
       (local dat (split m1 "."))
       (. dat 1))
     (= typ "s")
     (do
       (local dat (split s1 "."))
       (. dat 1)))))

;;(name)
(fn atentu.name []
  (local h (io.open "/etc/hostname"))
  (local h (h:read "*a"))
  (local h (string.gsub h "[\r\n]" ""))
  h)

(fn atentu.ip [dev]
  (local i (io.popen (.. "ip -br addr | grep " dev " | awk '{print $3}'")))
  (local i (i:read "*a"))
  (local i (string.gsub i "[\r\n]" ""))
  i)

(fn atentu.temp []
  (var avgtemp 0)
  (let
      [zones (io.popen "ls /sys/class/thermal | grep thermal_zone")
       temps []]
    (each [line (: zones "lines")]
      (do
        (with-open
            [f (io.open (.. "/sys/class/thermal/" line "/temp") "r")]
          (table.insert temps (/ (tonumber (f:read)) 1000)))))
    (each [k v (pairs temps)]
      (do
        (set avgtemp (+ avgtemp v))))
    (set avgtemp (/ avgtemp (# temps))))
  avgtemp)

;;(fn atentu.local_users_today []
;;  (let
;;      [us (io.popen (.. "grep \"pam_unix(login:session)\" /var/log/auth.log | grep $(date +%Y-%m-%d)"))]
;;    (each [line (: us "lines")]
;;      (do
;;        (let [ul (split line " ")]
;;          (print (.. (. (split (. ul 10) "(") 1) ": " (. ul 1) ":" (. ul 2))))))))
;;
;;(fn atentu.sshd_users_today [verbose?]
;;  (let
;;      [us (io.popen (.. "grep -E 'sshd\\[([0-9])+\\]: Accepted password for' /var/log/auth.log | grep $(date +%Y-%m-%d)"))]
;;    (each [line (: us "lines")]
;;      (do
;;        (if (= verbose? true)
;;            (do
;;              (let [ul (split line " ")]
;;                (print (.. (. (split (. ul 8) "(") 1) ": " (. ul 1) ":" (. ul 2) " " (. ul 10)))))
;;            (do
;;              (let [ul (split line " ")]
;;                (print (.. (. (split (. ul 8) "(") 1) ": " (. ul 1) ":" (. ul 2))))))))))

;;===== Main ====
atentu
