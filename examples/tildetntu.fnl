#!/usr/bin/fennel
(local atentu (require "atentu"))
(local lume (require "lume"))

(fn handlediscs [disctbl]
  (each [k v (pairs disctbl)]
         (do
           (print (.. v " ...: " (atentu.disc "used" v) "/" (atentu.disc "total" v))))))

(fn handleif [interfaces]
  (each [k v (pairs interfaces)]
    (do
      (print (.. v " ...: " (atentu.ip v))))))

(print (.. "=====[ " (atentu.name) " ]====="))
(print (.. "Load.....: " (atentu.loadavg 21)))
(print (.. "Memory.....: " (atentu.mem "used") "/" (atentu.mem "total")))
(print (.. "Uptime.....: " (atentu.uptime "d") " days " (atentu.uptime "h") " hours " (atentu.uptime "m") " min"))
(print "=====[ Network ]=====")
(handleif (lume.deserialize (. arg 2)))
(print "=====[ Disc Usage ]=====")
(print "Discs.....: ")
(handlediscs (lume.deserialize (. arg 1)))
(print "=====[ Online Today ]=====")
(atentu.sshd_users_today)
(atentu.local_users_today)
(print "=====[ droid.lambdacreate.tilde ]=====")
