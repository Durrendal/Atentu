#!/usr/bin/fennel
(local atentu (require "atentu"))

(print "====================")
(print (.. "Hostname.....: " (atentu.name)))
(print (.. "Load.....: " (atentu.loadavg 21)))
(print (.. "Memory.....: " (atentu.mem "used") "/" (atentu.mem "total")))
(print (.. "Uptime.....: " (atentu.uptime "d") " days " (atentu.uptime "h") " hours " (atentu.uptime "m") " min"))
(print "====================")
(print (.. "Discs.....: \n"
           "/ ... " (atentu.disc "used" "/") "/" (atentu.disc "total" "/")))
(print "====================")
(print "Access to this server is strictly prohibited.\nUnauthorized access will be persecuted to its fullest extent.")
(print "====================")
