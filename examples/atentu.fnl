#!/usr/bin/fennel
(local atentu (require "atentu"))
(local lume (require "lume"))

(fn usage []
  (print "Usage: 
atentu \"{'/', '/home'}\" \"{'eth0','wlan0'}\"
atentu [-h] [-m] [-u] [-t] [-lo] [-d \"{'/'}\"] [-i \"{'wlan0'}\"]
-m  |  Show memory usage
-u  |  Show uptime
-t  |  Show cpu temp
-lo |  Show system load
-d  |  Show disc usage
-i  |  Show interface ips"))

(fn handlediscs [disctbl]
  (each [k v (pairs disctbl)]
         (do
           (print (.. v " ...: " (atentu.disc "used" v) "/" (atentu.disc "total" v))))))

(fn handleif [interfaces]
    (each [k v (pairs interfaces)]
      (do
        (print (.. v " ...: " (atentu.ip v))))))

(fn motd []
  (print (.. "=====[ " (atentu.name) " ]====="))
  (print (.. "Load.....: " (atentu.loadavg 21)))
  (print (.. "Memory...: " (atentu.mem "used") "/" (atentu.mem "total")))
  (print (.. "Swap.....: " (atentu.swap "used") "/" (atentu.swap "total")))
  (print (.. "Temp.....: " (atentu.temp) "c"))
  (print (.. "Uptime...: " (atentu.uptime "d") " days " (atentu.uptime "h") " hours " (atentu.uptime "m") " min"))
  (print "=====[ Network ]=====")
  (handleif (lume.deserialize (. arg 2)))
  (print "=====[ Disc Usage ]=====")
  (print "Discs....: ")
  (handlediscs (lume.deserialize (. arg 1)))
  (print "=====[ WARN ]=====")
  (print "Access to this server is strictly prohibited.\nUnauthorized access will be persecuted to its fullest extent.")
  (print "=====[ WARN ]====="))

(fn main [?argv1 ?argv2]
  (if
   (or (= ?argv1 "-h") (= ?argv1 nil))
   (usage)
   (= ?argv1 "-hst")
   (print (atentu.name))
   (= ?argv1 "-m")
   (print (.. "Memory...: " (atentu.mem "used") "/" (atentu.mem "total")))
   (= ?argv1 "-lo")
   (print (.. "Load...: " (atentu.loadavg 21)))
   (= ?argv1 "-u")
   (print (.. "Uptime...: " (atentu.uptime "d") " days " (atentu.uptime "h") " hours " (atentu.uptime "m") " min"))
   (= ?argv1 "-s")
   (print (.. "Swap...: " (atentu.swap "used") "/" (atentu.swap "total")))
   (= ?argv1 "-d")
   (handlediscs (lume.deserialize ?argv2))
   (= ?argv1 "-i")
   (handleif (lume.deserialize (. arg 2)))
   (= ?argv1 "-t")
   (print (.. "Temp...: " (atentu.temp) "c"))
   (motd)))

(main ...)
